declare var rainbowSDK:any;
declare var angular:any;
declare var $:any;

/**
 * SDK
 * TypeScript Singleton
 * Centralize access to rainbowSDK global variable (avoid dispatching rainbowSDK anywhere in the application)
 */
class SDK {

    initialize() {

        console.log("[DEMO] :: Starter-Kit of the Rainbow SDK for Web with React started!");

        // Todo : do not put that in clear here.
        var applicationID = "346828d0ee3c11e8a85b99fd800e70f8",
            applicationSecret = "0SUgh1dDz58mM9e2hjypJKGwebaDhY77p3PgQi6TgUrC1BeuPTPzLzszNLsyBIok";

        angular.bootstrap(document, ["sdk"]).get("rainbowSDK");

        var onReady = function onReady() {
            console.log("[DEMO] :: On SDK Ready !");
        };

        var onLoaded = function onLoaded() {
            console.log("[DEMO] :: On SDK Loaded !");

            rainbowSDK.initialize(applicationID, applicationSecret).then(function() {
                console.log("[DEMO] :: Rainbow SDK is initialized!");
            }).catch(function() {
                console.log("[DEMO] :: Something went wrong with the SDK...");
            });
        };

        $(document).on(rainbowSDK.RAINBOW_ONREADY, onReady);
        $(document).on(rainbowSDK.RAINBOW_ONLOADED, onLoaded);
        rainbowSDK.load();
    }

    get version () {
        return rainbowSDK.version;
    }

    get connection () {
        return rainbowSDK.connection;
    }

    get rainbowSDK () {
        return rainbowSDK;
    }
};

let sdk = new SDK();

export { sdk } ;