import {Component, OnInit} from '@angular/core';
import { sdk } from '../modules/sdk';
import { DataService } from "./data.service";

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})

export class AppComponent implements  OnInit{
    constructor(private dataService: DataService) { }

    status:string;

    ngOnInit() {
        this.status = sdk.connection.getState();
        this.dataService.currentMessage.subscribe(message => this.status = message)
    }
}
