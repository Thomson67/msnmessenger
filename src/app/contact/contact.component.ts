import { Component, OnInit } from '@angular/core';
import { sdk } from '../../modules/sdk';
import { Router } from "@angular/router";
declare var toastr;
declare var $:any;

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

    constructor (private router: Router) {}

    searchContacts : Array<any> = [];
    contacts : Array<any> = [];
    messages : Array<string>;
    search : string;
    conversation : any;
    message : string;

    ngOnInit() {
        this.message = null;
        this.conversation = null;
        // Authentification pas gérée
        if (sdk.connection.getState() == "disconnected") {
            toastr.error("Vous n'êtes pas connecté !");
            this.router.navigate(['/']);
        }
        // Charger la liste de contact
        this.reloadContacts();
    }

    // Fonction de recherche
    searchByName() {
        this.searchContacts = null;
        sdk.rainbowSDK.contacts.searchByName(this.search).then((contacts) => {
            this.searchContacts = contacts;
        }).catch(function(err) {
            toastr.error("Erreur lors de la recherche du contact par nom");
            console.log(err);
        });
    }

    // Ajout d'un contact + reload list if success
    addContact(contact) {
        sdk.rainbowSDK.contacts.addToContactsList(contact).then((contact) => {
            toastr.success('Contact ajouté !');
            this.reloadContacts();
        }).catch(function(err) {
            toastr.error("Erreur lors de l'ajout de contact");
            console.log(err);
        });
    }

    // Suppression d'un contact + reload list if success
    deleteContact(contact) {
        sdk.rainbowSDK.contacts.removeFromContactsList(contact);
        this.reloadContacts();
    }

    reloadContacts() {
        this.contacts = sdk.rainbowSDK.contacts.getAll();
    }

    // Chargement d'une conversation + des 30 derniers messages
    loadConversation(contact) {
        sdk.rainbowSDK.conversations.openConversationForContact(contact).then((conversation) => {
            this.conversation = conversation;
            sdk.rainbowSDK.im.getMessagesFromConversation(this.conversation, 30);
            console.log(conversation);
        }).catch(function(err) {
            toastr.error("Erreur lors la récupération de la Conversation");
            console.log(err);
        });
    }

    // Envoi d'un message
    sendMessage() {
        sdk.rainbowSDK.im.sendMessageToConversation(this.conversation, this.message);
        this.message = null;
        this.loadConversation(this.conversation.contact);
    }

    // Son du wizz :)
    wizz() {
        let audio = new Audio();
        audio.src = "../../assets/msn-wizz.mp3";
        audio.load();
        audio.play();
    }
}
