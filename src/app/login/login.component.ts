import {Component, OnInit} from '@angular/core';
import { sdk } from '../../modules/sdk';
import { DataService } from "../data.service";
import {Router} from "@angular/router";
declare var toastr;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    constructor(private dataService: DataService, private router: Router) { }

    status:string;
    data = {};

    ngOnInit() {
        this.dataService.currentMessage.subscribe(message => this.status = message);
    }

    formSubmit() {
        sdk.connection.signin(this.data['username'], this.data['password'])
            .then((account) => {
                console.log(account);
                this.dataService.changeMessage("connected");
                toastr.success('Connecté !');
                this.router.navigate(['/contact']);
            })
            .catch(function(err) {
                console.log(err);
                toastr.error('Erreur lors de la connexion');
                // Todo : show error (currently outputs an HTTP access prompt, can't handle that ?!)
            });
    }
}
