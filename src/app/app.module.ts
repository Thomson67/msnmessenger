import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './contact/contact.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DataService } from "./data.service";

const appRoutes: Routes = [
    { path: 'contact', component: ContactComponent },
    { path: 'login', component: LoginComponent },
    { path: '', component: DashboardComponent, pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    LoginComponent,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(
        appRoutes
    )
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})

export class AppModule { }
